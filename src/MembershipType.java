public class MembershipType {
    private int pointsForMembership;
    String status;
    public MembershipType(int pointsForMembership) {
        this.pointsForMembership = pointsForMembership;
    }

    public int getPointsForMembership() {
        return pointsForMembership;
    }

    public void setPointsForMembership(int pointsForMembership) {
        this.pointsForMembership = pointsForMembership;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
