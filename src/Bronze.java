import java.time.LocalDateTime;
import java.time.LocalTime;

public class Bronze extends MembershipType{
    protected static final LocalTime ALLOWED_TRADE_HOUR = LocalTime.of(10, 00,00);
    public Bronze(int pointsForMembership) {
        super(pointsForMembership);

        if(pointsForMembership>=0 && pointsForMembership<10){
            System.out.println("Bronze Membership");
            this.setStatus("Bronze");
        }
    }
}
