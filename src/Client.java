import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDateTime;

public class Client {
    private String firstName;
    private String lastName;
    private MembershipType memberType;
    private double clientPoints;

    private double totalTradesPerDay;


    public Client(String firstName, String lastName, MembershipType memberType) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.memberType = memberType;
    }


    public void addTrade(Trade myTrade){

    }
    public boolean canTrade(){
    switch (this.memberType.getStatus()){
        case "Bronze":
            if(totalTradesPerDay<this.memberType.getPointsForMembership() && LocalDateTime.now().isAfter(ChronoLocalDateTime.from(Bronze.ALLOWED_TRADE_HOUR))){

                return true;
            }
        case "Silver":
                if(totalTradesPerDay<this.memberType.getPointsForMembership())
                    return true;
                break;
        case "Gold":
                return true;

        default:
            return false;
    }
    return false;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public MembershipType getMemberType() {
        return memberType;
    }

    public void setMemberType(MembershipType memberType) {
        this.memberType = memberType;
    }

    public double getClientPoints() {
        return clientPoints;
    }

    public void setClientPoints(double clientPoints) {
        this.clientPoints = clientPoints;
    }

}
