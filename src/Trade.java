import java.time.LocalDate;
import java.time.LocalDateTime;

public abstract class Trade {
    private String TID;
    private String symbol;
    private int quantity;
    private double price;
    private final LocalDateTime dateOfTrade = LocalDateTime.now();

    public Trade(String TID, String symbol, int quantity, double price){
        this.TID = TID;
        this.symbol = symbol;
        this.quantity = quantity;
        this.price = price;
    }

    public Trade(){

    }

    @Override
    public String toString() {
        return "The ID"+this.TID+"\n The Symbol: "+this.symbol+
                "\n The Quantity"+this.quantity+"\n The price: "+this.price;
    }

//    All Setters

//    public void setTID(String TID) {
//        this.TID = TID;
//    }
//
//    public void setSymbol(String symbol) {
//        this.symbol = symbol;
//    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setPrice(double price) {
        if(price>0)
            this.price = price;
        else
            System.out.println("Wrong input Value");
    }

//    All Getters

//    public String getTID() {
//        return TID;
//    }
//
//    public String getSymbol() {
//        return symbol;
//    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

//    Abstraction and Inheritance
    public abstract double calcDividend();
}
