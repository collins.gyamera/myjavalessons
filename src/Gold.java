public class Gold extends MembershipType{
    public Gold(int pointsForMembership) {
        super(pointsForMembership);

        if(pointsForMembership>=20){
            System.out.println("Gold Membership");
            this.setStatus("Gold");
        }
    }
}
