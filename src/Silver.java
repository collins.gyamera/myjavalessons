import java.time.LocalTime;

public class Silver extends MembershipType{
    protected static final LocalTime ALLOWED_TRADE_HOUR = LocalTime.of(10, 00,00);

    public Silver(int pointsForMembership) {
        super(pointsForMembership);

        if(pointsForMembership>=10 && pointsForMembership<19){
            System.out.println("Silver Membership");
            this.setStatus("Silver");
        }
    }
}
